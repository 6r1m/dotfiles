function fish_title;end

alias vi="nvim"
alias vim="nvim"

set GOROOT $HOME/coding/go/go
set -x GOPATH $HOME/coding/go
set PATH $PATH $GOROOT/bin $GOPATH/bin
set PATH $PATH /usr/local/opt/python/libexec/bin
set PATH $PATH $HOME/.cargo/bin

set FS $GOPATH/src/platformcraft/filespot
set CERN $GOPATH/src/platformcraft/cern

set RS $HOME/coding/rust

set TERM xterm-256color

set LANG ru_RU.UTF-8
set LC_COLLATE ru_RU.UTF-8
set LC_CTYPE ru_RU.UTF-8
set LC_MESSAGES ru_RU.UTF-8
set LC_MONETARY ru_RU.UTF-8
set LC_NUMERIC ru_RU.UTF-8
set LC_TIME ru_RU.UTF-8

if status is-interactive
and not set -q TMUX
    exec tmux attach
end

set -g prefix C-q
