set shell=/bin/bash
set nocompatible              " be improved
filetype off

if empty(glob('~/.vim/autoload/plug.vim'))
	silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs

				\ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
	autocmd VimEnter * PlugInstall | source $MYVIMRC
endif

call plug#begin('~/.vim/plugged')
Plug 'joshdick/onedark.vim'

" Plug 'autozimu/LanguageClient-neovim', {
"     \ 'branch': 'next',
"     \ 'do': 'bash install.sh',
"     \ }

Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
Plug 'scrooloose/nerdtree'
Plug 'jiangmiao/auto-pairs'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-commentary'

Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'zchee/deoplete-go', { 'for': 'go', 'do': 'make'}
Plug 'sebastianmarkow/deoplete-rust'

Plug 'godoctor/godoctor.vim', { 'do': ':GoDoctorInstall'}
Plug 'fatih/vim-go'

Plug 'rust-lang/rust.vim'

Plug 'majutsushi/tagbar'
Plug 'fisadev/FixedTaskList.vim'

Plug 'tpope/vim-repeat'
Plug 'airblade/vim-gitgutter'
Plug 'tpope/vim-fugitive'

Plug 'uarun/vim-protobuf'

Plug 'pangloss/vim-javascript'
Plug 'mattn/emmet-vim'
Plug 'gko/vim-coloresque'
Plug 'posva/vim-vue'

Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'

" All of your Plugins must be added before the following line
call plug#end()				 " required
filetype plugin indent on    " required

syntax on
set nu

" Ultisnps configuration
inoremap <silent><expr><TAB> pumvisible() ? "\<C-n>" : "\<tab>"
let g:UltiSnipsExpandTrigger="<c-j>"
let g:UltiSnipsJumpForwardTrigger="<c-k>"
" let g:UltiSnipsJumpForwardTrigger="<C-j>"
let g:UltiSnipsJumpBackwardTrigger="<S-tab>"


set ignorecase
set mouse=a
set nobackup
set noswapfile
set nowritebackup
set hlsearch

set ttyfast
set wildmenu
set showcmd

let mapleader="m"
set tabstop=4 softtabstop=0 noexpandtab shiftwidth=4
set backspace=indent,eol,start
set clipboard=unnamed

" Key bindings
vnoremap < <gv
vnoremap > >gv
nmap nt :tabnew<CR>
nmap <silent><leader>f :Autoformat<CR>

nmap <silent> <F4> :set invpaste<CR>:set paste?<CR>
imap <silent> <F4> <ESC>:set invpaste<CR>:set paste?<CR>

map <silent> <leader><leader>t <Plug>TaskList

map <silent><C-n> :NERDTreeToggle<CR>
nmap <silent>tb :TagbarToggle<CR>
let g:tagbar_width = 55

" Press space to clear search highlighting
nnoremap <silent> <Space> :noh<CR>
nnoremap <NL> i<CR><ESC>
nnoremap <C-p> :Files<CR>
nnoremap <Space>/ :Ag<CR>

" Neovim stuff
if has("nvim")
	tnoremap <Esc> <C-\><C-n>
	nmap <silent>tt  :10split<CR>:terminal<CR>
	let $NVIM_TUI_ENABLE_CURSOR_SHAPE = 1
else
	let &t_SI = "\<Esc>]50;CursorShape=1\x7" " Vertical bar in insert mode
	let &t_EI = "\<Esc>]50;CursorShape=0\x7" " Block in normal mode
endif

" Autocomplete configuration
let g:deoplete#enable_at_startup = 1
let g:deoplete#sources#go#sort_class = ['package', 'func', 'type', 'var', 'const']

set completeopt+=noselect
set completeopt-=preview


" Statusline settings
set laststatus=2
set statusline=%F%m%r%h%w\
set statusline+=%=%{fugitive#statusline()}\
set statusline+=[%{strlen(&fenc)?&fenc:&enc}]
set statusline+=\ [line\ %l\/%L]

" Rust stuff
let g:rustfmt_autosave = 1
au BufNewFile,BufRead *.rs let b:AutoPairs = {'(':')', '[':']', '{':'}','"':'"', '`':'`'}
let g:deoplete#sources#rust#racer_binary = '/Users/billyfbrain/.cargo/bin/racer'
let g:deoplete#sources#rust#rust_source_path = '/Users/billyfbrain/.rustup/toolchains/stable-x86_64-apple-darwin/lib/rustlib/src/rust/src/'

" set hidden
" let g:LanguageClient_serverCommands = {
"     \ 'rust': ['rustup', 'run', 'nightly', 'rls'],
"     \ }
" let g:LanguageClient_rootMarkers = {
" 	\ 'rust': ['Cargo.toml'],
" 	\ }
nnoremap <F5> :call LanguageClient_contextMenu()<CR>
nnoremap <silent> gd :call LanguageClient#textDocument_definition()<CR>
nnoremap <silent> <F2> :call LanguageClient#textDocument_rename()<CR>

" Go stuff
let g:go_fmt_command = "goimports"
au BufNewFile,BufRead *.go nnoremap <buffer> <silent> K :GoDoc<cr>
au BufNewFile,BufRead *.go nnoremap <buffer> <silent> gi :GoInfo<cr>
au BufNewFile,BufRead *.go nnoremap <buffer> <silent> gt :GoTest<cr>

" YAML
autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab


" Theme
colorscheme onedark
" set background=dark

"Credit joshdick
"Use 24-bit (true-color) mode in Vim/Neovim when outside tmux.
"If you're using tmux version 2.2 or later, you can remove the outermost $TMUX check and use tmux's 24-bit color support
"(see < http://sunaku.github.io/tmux-24bit-color.html#usage > for more information.)
if (!empty($TMUX))
	" set t_8b=^[[48;2;%lu;%lu;%lum
	" set t_8f=^[[38;2;%lu;%lu;%lum
	if (has("nvim"))
	"For Neovim 0.1.3 and 0.1.4 < https://github.com/neovim/neovim/pull/2198 >
		let $NVIM_TUI_ENABLE_TRUE_COLOR=1
	endif
  "For Neovim > 0.1.5 and Vim > patch 7.4.1799 < https://github.com/vim/vim/commit/61be73bb0f965a895bfb064ea3e55476ac175162 >
  "Based on Vim patch 7.4.1770 (`guicolors` option) < https://github.com/vim/vim/commit/8a633e3427b47286869aa4b96f2bfc1fe65b25cd >
  " < https://github.com/neovim/neovim/wiki/Following-HEAD#20160511 >
	if (has("termguicolors"))
		set termguicolors
	endif
endif
