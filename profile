source ~/.fzf.bash
## Golang variables
export GOROOT=$HOME/coding/go/go
export GOPATH=$HOME/coding/go
export PATH=$GOROOT/bin:$GOPATH/bin:$PATH

# Brew stuff
export PATH=/usr/local/opt/python/libexec/bin:$PATH

# GIT
export PATH=/usr/local/git/bin:/usr/local/bin:/usr/bin:/usr/local/sbin:$PATH
source ~/.git-completion.bash

#Platfoemcraft services
#Filespot
export FS=$GOPATH/src/platformcraft/filespot
export CERN=$GOPATH/src/platformcraft/cern
export PPRFNK=$HOME/coding/python/pprfnk

# Rust
export PATH="$HOME/.cargo/bin:$PATH"

# Heroku
export PATH="/usr/local/heroku/bin:$PATH"

# ## Syntx highlighting
export CLICOLOR=cons25
export CLICOLOR=1
export LSCOLORS=ExFxBxDxCxegedabagacad

# Git brunch
git branch --no-color 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\ \[\1\]/'

function parse_git_branch() {
    br=`git branch --no-color 2>/dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\ \(\1\)/'`
    if [ "$br" != "" ];   then
        br="$br"
        echo $br" "
    fi
}

## Colorized
COLOR_RED='\[\e[1;31m\]'
COLOR_GREEN='\[\e[1;32m\]'
NONE_COLOR='\[\e[0m\]'
BLUE='\[\e[1;34m\]'
export PS1="${COLOR_GREEN}➔ ${NONE_COLOR}\W ${BLUE}\$(parse_git_branch)${COLOR_GREEN}$ \[$(tput sgr0)\]"

## Aliasess
alias vim='nvim --cmd "lang en_US"'
alias py='python'
alias py3='python3'
alias venv='virtualenv --no-site-packages'

export PATH="$HOME/.cargo/bin:$PATH"
