set shell=/bin/bash
set nocompatible              " be improved
filetype off

if empty(glob('~/.vim/autoload/plug.vim'))
	silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs

				\ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
	autocmd VimEnter * PlugInstall | source $MYVIMRC
endif

call plug#begin('~/.vim/plugged')

Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'scrooloose/nerdtree'
Plug 'jiangmiao/auto-pairs'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-commentary'

Plug 'neoclide/coc.nvim', {'tag': '*', 'branch': 'release'}

Plug 'godoctor/godoctor.vim', { 'do': ':GoDoctorInstall'}
Plug 'fatih/vim-go'

Plug 'rust-lang/rust.vim'

Plug 'majutsushi/tagbar'
Plug 'ludovicchabant/vim-gutentags'
Plug 'fisadev/FixedTaskList.vim'

Plug 'tpope/vim-repeat'
Plug 'airblade/vim-gitgutter'
Plug 'tpope/vim-fugitive'

Plug 'pangloss/vim-javascript'
Plug 'posva/vim-vue'
Plug 'gko/vim-coloresque'

Plug 'honza/vim-snippets'

Plug 'rakr/vim-one'

" All of your Plugins must be added before the following line
call plug#end()				 " required
filetype plugin indent on    " required

syntax on
set nu

" autocmd BufEnter * call ncm2#enable_for_buffer()
set completeopt=noinsert,menuone,noselect

inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

inoremap <silent><expr> <c-u> coc#refresh()
inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
" inoremap <silent><expr> <cr> pumvisible() ? coc#_select_confirm() : "\<C-g>u\<CR>"

" Better display for messages
set cmdheight=1

" Smaller updatetime for CursorHold & CursorHoldI
set updatetime=300

" don't give |ins-completion-menu| messages.
set shortmess+=c

" always show signcolumns
set signcolumn=yes
set ignorecase
set mouse=a
set nobackup
set noswapfile
set nowritebackup
set hlsearch

set ttyfast
set wildmenu
set showcmd

let mapleader="m"
set tabstop=4 softtabstop=0 noexpandtab shiftwidth=4
set backspace=indent,eol,start
set clipboard=unnamed

" Key bindings
vnoremap < <gv
vnoremap > >gv
nmap nt :tabnew<CR>
nmap <silent><leader>f :Autoformat<CR>

nmap <silent> <F4> :set invpaste<CR>:set paste?<CR>
imap <silent> <F4> <ESC>:set invpaste<CR>:set paste?<CR>

map <silent> <leader><leader>t <Plug>TaskList

map <silent><C-n> :NERDTreeToggle<CR>
nmap <silent>tb :TagbarToggle<CR>
let g:tagbar_width = 55

" Press space to clear search highlighting
nnoremap <silent> <Space> :noh<CR>
nnoremap <NL> i<CR><ESC>
nnoremap <C-p> :Files<CR>
nnoremap <Space>/ :Ag<CR>

language en_US
set guicursor=

set completeopt-=preview


" Statusline settings
set laststatus=2
set statusline=%F%m%r%h%w\
set statusline+=%=%{fugitive#statusline()}\
set statusline+=[%{strlen(&fenc)?&fenc:&enc}]
set statusline+=\ [line\ %l\/%L]

" Rust stuff
let g:rustfmt_autosave = 1
au BufNewFile,BufRead *.rs let b:AutoPairs = {'(':')', '[':']', '{':'}','"':'"', '`':'`'}
 let g:tagbar_type_rust = {
    \ 'ctagstype' : 'rust',
    \ 'kinds' : [
        \'n:modules',
        \'s:struct',
        \'i:interface',
        \'c:impl',
        \'f:functions, func',
        \'g:enum',
        \'t:typedef',
        \'v:var',
        \'M:macro',
        \'m:fields',
        \'e:enum variant',
        \'P:methods',
    \]
    \}
let g:rust_use_custom_ctags_defs = 1

set hidden

" Remap keys for gotos
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)
" Remap for rename current word
nmap <leader>rn <Plug>(coc-rename)

" Go stuff
let g:go_fmt_command = "goimports"
au BufNewFile,BufRead *.go nnoremap <buffer> <silent> K :GoDoc<cr>
au BufNewFile,BufRead *.go nnoremap <buffer> <silent> gi :GoInfo<cr>
au BufNewFile,BufRead *.go nnoremap <buffer> <silent> gt :GoTest<cr>

" YAML
autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab


" Theme
if (has("termguicolors"))
  set termguicolors
endif
" colorscheme spacemacs-theme
set background=dark

colorscheme one

"Credit joshdick
"Use 24-bit (true-color) mode in Vim/Neovim when outside tmux.
"If you're using tmux version 2.2 or later, you can remove the outermost $TMUX check and use tmux's 24-bit color support
"(see < http://sunaku.github.io/tmux-24bit-color.html#usage > for more information.)
if (!empty($TMUX))
	" set t_8b=^[[48;2;%lu;%lu;%lum
	" set t_8f=^[[38;2;%lu;%lu;%lum
	if (has("nvim"))
	"For Neovim 0.1.3 and 0.1.4 < https://github.com/neovim/neovim/pull/2198 >
		let $NVIM_TUI_ENABLE_TRUE_COLOR=1
	endif
  "For Neovim > 0.1.5 and Vim > patch 7.4.1799 < https://github.com/vim/vim/commit/61be73bb0f965a895bfb064ea3e55476ac175162 >
  "Based on Vim patch 7.4.1770 (`guicolors` option) < https://github.com/vim/vim/commit/8a633e3427b47286869aa4b96f2bfc1fe65b25cd >
  " < https://github.com/neovim/neovim/wiki/Following-HEAD#20160511 >
	if (has("termguicolors"))
		set termguicolors
	endif
endif
